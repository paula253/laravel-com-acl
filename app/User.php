<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Permission;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function roles()
    {
        return $this->belongsToMany(\App\Role::class);    
    }
    
    
    
    //Pessoas que podem fazer determinada ação(permissões).
    public function hasPermission(Permission $permission)
    {
        return $this->hasAnyRoles($permission->roles);
        //Exemplo:
        //view_post=>Manager,Editor
    }
    
    //verificar se o usuario logado tem essa função especifica.
    public function hasAnyRoles($roles)
    {
        //Estou olhando se é um array ou um objeto.
        if(is_array($roles) || is_object($roles))
        {
            return !! $roles->intersect($this->roles)->count();
        }
        
        //Possui apenas um item
        return $this->roles->contains('name',$roles);
    }
}