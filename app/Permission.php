<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //Retorna todas as regras(rules) que estão vinculadas a essas permissões.
    public function roles()
    {
        return $this->belongsToMany(\App\Role::class);
    }
}
