<?php

namespace App\Providers;


//use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

//precisei colocar essa classe
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

use App\Post;
use App\User;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        
        // \App\Post::class => \App\Policies\PostPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        
       $this->registerPolicies($gate);
        //se colocar User $user ele consegue pegar o usuário ja logado.
      
      /*  $gate->define('update-post',function(User $user,Post $post)
        {
            
            return $user->id == $post->user_id;
            
        });*/
        
        $permissions = Permission::with('roles')->get();
        //Metodo encontrado em model Permission.
        //Retorna: Metodo e papel vinculado.
        //view_post=>Manager,Editor
        //delete_post=>Manager,
        //edit_post=>Manager 
        
        foreach($permissions as $permission)
        {
            $gate->define($permission->name,function(User $user) use ($permission)
            {
                return $user->hasPermission($permission);
            }); 
        }
                     
        //Essa função olha primeiro se o usuário é adm    
        $gate->before(function(User $user)
        {
            if ($user->hasAnyRoles('adm'))
            {
                return true;
            }
        });
    }
}
