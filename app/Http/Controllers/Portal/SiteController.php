<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;

use App\Post;
use Gate;
use App\Http\Controllers\Controller;//apenas porque renomei.

class SiteController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        /*
      //aparecer todos os posts 
      $posts = $post->all();//validado na propria view.
      
      //aparecer apenas os postos do usario logado
     // $posts = $post->where('user_id',auth()->user()->id)->get();
      
      return view('home',compact('posts'));
      */
      
      return view('portal.home.index');
      
    }
    
    public function update($idPost)
    {
        
     //   dd($idPost);
        
        $post=Post::find($idPost); 
       /* uma forma de autentificar----------------  
        $this->authorize('update-post',$post);
        ------------------------------------------*/ 
        
        
        if(Gate::denies('update-post',$post)) 
            abort(403,'Não autorizado!');
        
        
        return view('update-post',compact('post'));
    }
     

    public function rolesPermissions()
    {
        $name=auth()->user()->name;    
        echo("<h1>$name</h1>");
        
        foreach(auth()->user()->roles as $role)
        {
            echo "<b>$role->name</b> ->";
            
            $permissions= $role->permissions;
            foreach($permissions as $permission)
            {
                echo "$permission->name,";
            }
         
         echo "<hr>";   
        }


    }

}