<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Post; //Para utlisar a model pots.

class PostController extends Controller
{
    
    private $post;
    
    //variavel this recebendo os posts.
    public function __construct(Post $post)
    {
        $this->post=$post;
        
    }
 
    public function index()
    {
        $posts = $this->post->all();
        
        return view('painel.posts.index',compact('posts'));
    }
}
