<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //muitos para um (muitos posts para um usuario)
    
    public function user()
    {
        return $this->belongsTo(User::class);
        //não precisa especificar o campo,pois esta utilizando o padrão do laravel'id_user'
    }
}
